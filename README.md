# Big Text File

This repository contains a predownloaded version of 470MB uncompressed of the
first 1000 books of Gutenberg project (file gutenberg-utf-8.txt.bz2). 

To uncompress, just run:

    $ bzip2 -d gutenberg-utf-8.txt.bz2

If you wish to redownload, just run:

    $ python3 gutenberg-download.py

Used as input for [triplet_challenge](https://github.com/pamarcos/triplet_challenge)

This is just done for academic personal research & for fun.