import requests

# http://www.gutenberg.org/dirs/GUTINDEX.ALL

gutenberg_url = 'http://www.gutenberg.org'

with open('gutenberg-utf-8.txt', 'a+b') as f:
  for i in range (1, 1000):
    print (f"Downloading book {i}...")
    try:
      with requests.get (f'{gutenberg_url}/ebooks/{i}.txt.utf-8', stream = True) as r:
        r.raise_for_status()
        for chunk in r.iter_content(chunk_size=8192):
          if chunk: # filter out keep-alive new chunks
            f.write(chunk)
    except:
      # skip broken links or unavailable at this time
      print (f"Error: Skipping {i}")
      pass